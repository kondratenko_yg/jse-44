package ru.kondratenko.jse44;

import ru.kondratenko.jse44.controller.IControllerCustomerImpl;
import ru.kondratenko.jse44.controller.IControllerAccountImpl;

import jakarta.xml.ws.Endpoint;

public class Application {

    public static void main(final String[] args) {
        IControllerCustomerImpl customerController = new IControllerCustomerImpl();
        Endpoint.publish("http://localhost:8899/ws/customer", customerController);

        IControllerAccountImpl accountController = new IControllerAccountImpl();
        Endpoint.publish("http://localhost:8899/ws/account", accountController);

    }

}
