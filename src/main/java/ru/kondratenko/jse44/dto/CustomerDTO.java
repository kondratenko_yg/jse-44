package ru.kondratenko.jse44.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomerDTO {
    public static final Long serialVersionUID = 1L;


    private Integer id;

    private String firstName;

    private String lastName;

    private String birthDate ;
}
