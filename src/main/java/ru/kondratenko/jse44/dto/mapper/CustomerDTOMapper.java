package ru.kondratenko.jse44.dto.mapper;

import ru.kondratenko.jse44.dto.CustomerDTO;
import ru.kondratenko.jse44.entity.Customer;

import java.text.SimpleDateFormat;

public class CustomerDTOMapper {
    public static CustomerDTO toDto(Customer customer) {
        CustomerDTO customerDTO = CustomerDTO.builder()
                .id(customer.getId())
                .firstName(customer.getFirstName())
                .lastName(customer.getLastName())
                .build();
        if (customer.getBirthDate() != null) {
            customerDTO.setBirthDate(customer.getBirthDate().toString());
        }
        return customerDTO;
    }

    public static Customer fromDto(CustomerDTO customerDTO) {
        Customer customer = Customer.builder()
                .id(customerDTO.getId())
                .firstName(customerDTO.getFirstName())
                .lastName(customerDTO.getLastName())
                .build();
        if (customerDTO.getBirthDate() != null) {
            try{
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                customer.setBirthDate(formatter.parse(customerDTO.getBirthDate()));
            }catch(Exception e){}

        }
        return customer;
    }
}

