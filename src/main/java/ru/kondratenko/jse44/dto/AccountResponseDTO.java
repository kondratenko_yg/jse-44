package ru.kondratenko.jse44.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.kondratenko.jse44.entity.Account;
import ru.kondratenko.jse44.enumerated.Status;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccountResponseDTO {
    private Status status;
    private Account payloadAccount;
}
