package ru.kondratenko.jse44.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.kondratenko.jse44.entity.Account;
import ru.kondratenko.jse44.entity.Customer;
import ru.kondratenko.jse44.entity.Item;
import ru.kondratenko.jse44.enumerated.Status;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ItemResponse {
    private Status status;
    private Item[] payloadItem;
}
