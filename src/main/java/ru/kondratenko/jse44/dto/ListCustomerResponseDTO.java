package ru.kondratenko.jse44.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.kondratenko.jse44.enumerated.Status;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ListCustomerResponseDTO {
    private Status status;
    private CustomerDTO[] payloadCustomer;

}
