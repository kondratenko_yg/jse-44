package ru.kondratenko.jse44.repository;

import ru.kondratenko.jse44.entity.Account;
import ru.kondratenko.jse44.entity.Customer;
import ru.kondratenko.jse44.entity.Item;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

public interface IAccountRepository extends IRepository<Account> {
    Optional<Account> findByAccountNumber(String id);
    void removeByAccountNumber(String id);
}
