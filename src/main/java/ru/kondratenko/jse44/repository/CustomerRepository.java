package ru.kondratenko.jse44.repository;

import org.apache.ibatis.session.SqlSession;
import ru.kondratenko.jse44.entity.Customer;
import ru.kondratenko.jse44.entity.Item;
import ru.kondratenko.jse44.repository.datasource.MyBatisUtil;
import ru.kondratenko.jse44.repository.mapper.AccountMBMapper;
import ru.kondratenko.jse44.repository.mapper.CustomerMBMapper;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public class CustomerRepository implements ICustomerRepository{

    private static volatile CustomerRepository instance;

    private CustomerRepository() {
    }

    public static CustomerRepository getInstance(){
        CustomerRepository localInstance = instance;
        if (localInstance == null) {
            synchronized (CustomerRepository.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new CustomerRepository();
                }
            }
        }
        return localInstance;
    }

    @Override
    public Optional<Customer> create(Customer input) {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            CustomerMBMapper customerMBMapper = sqlSession.getMapper(CustomerMBMapper.class);
            customerMBMapper.save(input);
            sqlSession.commit();
            return Optional.of(input);
        }
    }

    @Override
    public Optional<Customer> update(Customer input) {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            CustomerMBMapper customerMBMapper = sqlSession.getMapper(CustomerMBMapper.class);
            customerMBMapper.update(input);
            sqlSession.commit();
            return Optional.of(input);
        }
    }

    @Override
    public Optional<Customer> findById(Integer id) {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            CustomerMBMapper customerMBMapper = sqlSession.getMapper(CustomerMBMapper.class);
            return Optional.ofNullable(customerMBMapper.getById(id));
        }
    }

    @Override
    public void removeById(Integer id){
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            CustomerMBMapper customerMBMapper = sqlSession.getMapper(CustomerMBMapper.class);
            customerMBMapper.deleteById(id);
            sqlSession.commit();
        }
    }

    @Override
    public List<Customer> findAll() {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            CustomerMBMapper customerMBMapper = sqlSession.getMapper(CustomerMBMapper.class);
            return customerMBMapper.getAllCustomers();
        }
    }

    @Override
    public List<Customer> findItems(Integer id, String firsName, String lastName, Date bDay, String account, Double balance) {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            CustomerMBMapper customerMBMapper = sqlSession.getMapper(CustomerMBMapper.class);
            return customerMBMapper.findItems(id, firsName, lastName,  bDay,  account,  balance);
        }
    }

    @Override
    public List<Item> findCombItems(Integer id, String firsName, String lastName, Date bDay, String account, Double balance) {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            CustomerMBMapper customerMBMapper = sqlSession.getMapper(CustomerMBMapper.class);
            return customerMBMapper.findCombItems(id, firsName, lastName,  bDay,  account,  balance);
        }
    }
    
}
