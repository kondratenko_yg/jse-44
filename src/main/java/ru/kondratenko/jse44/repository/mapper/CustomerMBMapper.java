package ru.kondratenko.jse44.repository.mapper;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.DateTypeHandler;
import ru.kondratenko.jse44.entity.Account;
import ru.kondratenko.jse44.entity.Customer;
import ru.kondratenko.jse44.entity.Item;
import ru.kondratenko.jse44.repository.mapper.provider.Provider;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public interface CustomerMBMapper {
    @Insert("insert into customer(first_name, last_name, birth_date) values " +
            "(#{firstName}, #{lastName},#{birthDate, typeHandler=org.apache.ibatis.type.DateTypeHandler})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    Integer save(Customer customer);

    @Update("update customer set " +
            "first_name = #{firstName}, last_name = #{lastName}, " +
            "birth_date = #{birthDate, typeHandler=org.apache.ibatis.type.DateTypeHandler}" +
            " where id  = #{id}")
    void update(Customer customer);

    @Delete("delete from customer where id = #{id}")
    void deleteById(Integer id);

    @Select("select * from customer where id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "birthDate", column = "birth_date", typeHandler = DateTypeHandler.class)
    })
    Customer getById(Integer id);

    @Select("select * from customer")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "birthDate", column = "birth_date", typeHandler = DateTypeHandler.class)
    })
    List<Customer> getAllCustomers();

    @SelectProvider(type = Provider.class, method = "findItems")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "birthDate", column = "birth_date", typeHandler = DateTypeHandler.class)
    })
    List<Customer> findItems(Integer id, String firsName, String lastName, Date bDay, String account, Double balance);

    //returns all accounts for customer
    @SelectProvider(type = Provider.class, method = "findItems")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "birthDate", column = "birth_date", typeHandler = DateTypeHandler.class),
            @Result(property = "account",column = "id", javaType = List.class, many = @Many(select = "ru.kondratenko.jse44.repository.mapper.AccountMBMapper.getById"))
    })
    List<Item> findCombItems(Integer id, String firsName, String lastName, Date bDay, String account, Double balance);
}
