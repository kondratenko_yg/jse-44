package ru.kondratenko.jse44.repository.mapper.provider;

import org.apache.ibatis.jdbc.SQL;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Provider {
    public String findItems(Integer id, String firsName, String lastName, Date bDay, String account, Double balance) {
        SQL sql = new SQL();
        sql.SELECT("customer.id as id",
                "account.ballance as ballance",
                "account.account_number as account_number",
                "customer.first_name as first_name",
                "customer.last_name as last_name",
                "customer.birth_date as birth_date");
        sql.FROM("customer");
        sql.LEFT_OUTER_JOIN("account on customer.id=account.customer_id");
        return parseCondition(sql, id, firsName, lastName, bDay, account, balance).toString();
    }

    private SQL parseCondition(SQL sql, Integer id, String firsName, String lastName, Date bDay, String account, Double balance) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        boolean hasCondition = false;
        if (id != null && id > 0) {
            sql.WHERE("account.customer_id ='" + id + "'");
            hasCondition = true;
        }
        if (firsName != null && !firsName.isEmpty()) {
            if (hasCondition) {
                sql.AND();
            }
            sql.WHERE("customer.first_name like '%" + firsName + "%'");
            hasCondition = true;
        }
        if (lastName != null && !lastName.isEmpty()) {
            if (hasCondition) {
                sql.AND();
            }
            sql.WHERE("customer.last_name like '%" + lastName + "%'");
            hasCondition = true;
        }
        if (bDay != null) {
            if (hasCondition) {
                sql.AND();
            }
            sql.WHERE("customer.birth_date ='" + formatter.format(bDay) + "'");
            hasCondition = true;
        }
        if (account != null && !account.isEmpty()) {
            if (hasCondition) {
                sql.AND();
            }
            sql.WHERE("account.account_number  ='" + account + "'");
        }
        if (balance != null) {
            sql.WHERE("account.ballance  ='" + balance + "'");
        }
        return sql;
    }
}
