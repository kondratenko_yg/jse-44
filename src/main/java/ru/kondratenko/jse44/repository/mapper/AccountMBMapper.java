package ru.kondratenko.jse44.repository.mapper;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.DateTypeHandler;
import ru.kondratenko.jse44.entity.Account;
import ru.kondratenko.jse44.entity.Item;
import ru.kondratenko.jse44.repository.mapper.provider.Provider;

import java.util.Date;
import java.util.List;

public interface AccountMBMapper {
    @Insert("insert into account(customer_id, account_number, ballance) values (#{customerId}, #{accountNumber}, #{balance})")
    Integer save(Account account);

    @Update("update account set customer_id = #{customerId}, account_number = #{accountNumber}, ballance = #{balance} where account_number = #{accountNumber}")
    void update(Account account);

    @Delete("delete from account where account_number = #{accountNumber}")
    void deleteByAccount(String accountNumber);

    @Select("select * from account where account_number = #{accountNumber}")
    @Results({
            @Result(property = "customerId", column = "customer_id"),
            @Result(property = "balance", column = "ballance"),
            @Result(property = "accountNumber", column = "account_number")
    })
    Account getByAccountNumber(String accountNumber);

    @Select("select * from account where customer_id = #{customerId}")
    @Results({
            @Result(property = "customerId", column = "customer_id"),
            @Result(property = "balance", column = "ballance"),
            @Result(property = "accountNumber", column = "account_number")
    })
    Account getById(Integer customerId);

    @Select("select * from account")
    @Results({
            @Result(property = "customerId", column = "customer_id"),
            @Result(property = "balance", column = "ballance"),
            @Result(property = "accountNumber", column = "account_number")
    })
    List<Account> getAllAccounts();

    @SelectProvider(type = Provider.class, method = "findItems")
    @Results({
            @Result(property = "customerId", column = "id"),
            @Result(property = "balance", column = "ballance"),
            @Result(property = "accountNumber", column = "account_number")
    })
    List<Account> findAccounts(Integer id, String firsName, String lastName, Date bDay, String account, Double balance );

    //returns filtered accounts for customer
    @SelectProvider(type = Provider.class, method = "findItems")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "birthDate", column = "birth_date", typeHandler = DateTypeHandler.class),
            @Result(property = "account",column = "account_number", javaType = List.class, many = @Many(select = "getByAccountNumber"))
    })
    List<Item> findCombItems(Integer id, String firsName, String lastName, Date bDay, String account, Double balance);
}
