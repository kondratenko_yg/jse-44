package ru.kondratenko.jse44.repository;

import org.apache.ibatis.session.SqlSession;
import ru.kondratenko.jse44.entity.Account;
import ru.kondratenko.jse44.entity.Customer;
import ru.kondratenko.jse44.entity.Item;
import ru.kondratenko.jse44.repository.datasource.MyBatisUtil;
import ru.kondratenko.jse44.repository.mapper.AccountMBMapper;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

public class AccountRepository implements IAccountRepository {

    private static volatile AccountRepository instance ;

    private AccountRepository() {
    }

    public static AccountRepository getInstance(){
        AccountRepository localInstance = instance;
        if (localInstance == null) {
            synchronized (AccountRepository.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new AccountRepository();
                }
            }
        }
        return localInstance;
    }

    @Override
    public Optional<Account> create(Account input) {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            AccountMBMapper accountMBMapper = sqlSession.getMapper(AccountMBMapper.class);
            accountMBMapper.save(input);
            sqlSession.commit();
            return Optional.of(input);
        }
    }

    @Override
    public Optional<Account> update(Account input) {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            AccountMBMapper accountMBMapper = sqlSession.getMapper(AccountMBMapper.class);
            accountMBMapper.update(input);
            sqlSession.commit();
            return Optional.of(input);
        }
    }

    @Override
    public Optional<Account> findByAccountNumber(String id) {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            AccountMBMapper accountMBMapper = sqlSession.getMapper(AccountMBMapper.class);
            return Optional.ofNullable(accountMBMapper.getByAccountNumber(id));
        }
    }

    @Override
    public void removeByAccountNumber(String id){
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            AccountMBMapper accountMBMapper = sqlSession.getMapper(AccountMBMapper.class);
            accountMBMapper.deleteByAccount(id);
            sqlSession.commit();
        }
    }

    @Override
    public List<Account> findAll() {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            AccountMBMapper accountMBMapper = sqlSession.getMapper(AccountMBMapper.class);
            return accountMBMapper.getAllAccounts();
        }
    }

    @Override
    public List<Account> findItems(Integer id, String firsName, String lastName, Date bDay, String account, Double balance) {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            AccountMBMapper accountMBMapper = sqlSession.getMapper(AccountMBMapper.class);
            return accountMBMapper.findAccounts(id, firsName, lastName,  bDay,  account,  balance);
        }
    }

    @Override
    public List<Item> findCombItems(Integer id, String firsName, String lastName, Date bDay, String account, Double balance) {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSession()) {
            AccountMBMapper accountMBMapper = sqlSession.getMapper(AccountMBMapper.class);
            return accountMBMapper.findCombItems(id, firsName, lastName,  bDay,  account,  balance);
        }
    }

}
