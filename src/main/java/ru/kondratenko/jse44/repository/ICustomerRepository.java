package ru.kondratenko.jse44.repository;

import ru.kondratenko.jse44.entity.Customer;

import java.util.Optional;

public interface ICustomerRepository extends IRepository<Customer> {
    Optional<Customer> findById(final Integer id);
    void removeById(final Integer id);
}
