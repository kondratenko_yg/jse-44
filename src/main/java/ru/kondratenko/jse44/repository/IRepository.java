package ru.kondratenko.jse44.repository;

import ru.kondratenko.jse44.entity.Item;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface IRepository<E> {
    Optional<E> create(final E item);
    List<E> findAll();
    Optional<E> update(final E item);
    List<E> findItems(Integer id, String firsName, String lastName, Date bDay, String account, Double balance);
    List<Item> findCombItems(Integer id, String firsName, String lastName, Date bDay, String account, Double balance);
}
