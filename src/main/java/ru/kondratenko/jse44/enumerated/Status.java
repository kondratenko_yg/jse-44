package ru.kondratenko.jse44.enumerated;

public enum Status {
    OK("Operation successful"),
    DB_ERROR("Database error"),
    PARAMS_MISMATCH("Parameters amount doesn't match requirements");

    private String message;

    Status(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
