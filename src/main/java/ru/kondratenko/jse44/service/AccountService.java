package ru.kondratenko.jse44.service;

import ru.kondratenko.jse44.dto.AccountResponseDTO;
import ru.kondratenko.jse44.dto.CustomerDTO;
import ru.kondratenko.jse44.dto.ItemResponse;
import ru.kondratenko.jse44.dto.ListAccountResponseDTO;
import ru.kondratenko.jse44.dto.mapper.CustomerDTOMapper;
import ru.kondratenko.jse44.entity.Account;
import ru.kondratenko.jse44.entity.Item;
import ru.kondratenko.jse44.enumerated.Status;
import ru.kondratenko.jse44.repository.AccountRepository;

import java.util.*;


public class AccountService implements IAccountIService {

    private final AccountRepository accountRepository;

    private static volatile AccountService instance;

    private AccountService() {
        this.accountRepository = AccountRepository.getInstance();
    }

    public static AccountService getInstance() {
        AccountService localInstance = instance;
        if (localInstance == null) {
            synchronized (AccountService.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new AccountService();
                }
            }
        }
        return localInstance;
    }

    @Override
    public AccountResponseDTO create(final Account account) {
        Account account1 = Account.builder()
                .customerId(account.getCustomerId())
                .accountNumber(account.getAccountNumber())
                .balance(account.getBalance())
                .build();
        Optional<Account> accountOptional = accountRepository.create(account);
        if (accountOptional.isPresent()) {
            return AccountResponseDTO.builder().payloadAccount(accountOptional.get()).status(Status.OK).build();
        }
        return AccountResponseDTO.builder().status(Status.DB_ERROR).build();
    }


    @Override
    public AccountResponseDTO updateByAccountNumber(Account account) {
        Optional<Account> account1 = accountRepository.findByAccountNumber(account.getAccountNumber());
        if (account1.isPresent()) {
            Account updatedAccount = Account.builder()
                    .accountNumber(account.getAccountNumber())
                    .customerId(account.getCustomerId())
                      .balance(account.getBalance())
                    .build();
            account1 = accountRepository.update(updatedAccount);
            if (account1.isPresent()) {
                return AccountResponseDTO.builder().payloadAccount(account1.get()).status(Status.OK).build();
            }
        }
        return AccountResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public AccountResponseDTO findByAccountNumber(String accountNumber) {
        Optional<Account> account1 = accountRepository.findByAccountNumber(accountNumber);
        if (account1.isPresent()) {
            return AccountResponseDTO.builder().payloadAccount(account1.get()).status(Status.OK).build();
        }
        return AccountResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public void removeByAccountNumber(String accountNumber) {
        accountRepository.removeByAccountNumber(accountNumber);
    }

    @Override
    public ListAccountResponseDTO findAll() {
        return ListAccountResponseDTO
                .builder()
                .payloadAccount(accountRepository.findAll().toArray(new Account[0]))
                .status(Status.OK).build();
    }

    @Override
    public ListAccountResponseDTO findAccounts(Integer id, String firsName, String lastName, Date bDay, String account, Double balance) {
        return ListAccountResponseDTO
                .builder()
                .payloadAccount(accountRepository.findItems(id, firsName, lastName,  bDay,  account,  balance).toArray(new Account[0]))
                .status(Status.OK).build();
    }

    @Override
    public ItemResponse findCustomersAccounts(Integer id, String firsName, String lastName, Date bDay, String account, Double balance) {
        return ItemResponse
                .builder()
                .payloadItem(accountRepository.findCombItems(id, firsName, lastName,  bDay,  account,  balance)
                        .toArray(Item[]::new)
                )
                .status(Status.OK).build();
    }
}
