package ru.kondratenko.jse44.service;

import ru.kondratenko.jse44.dto.AccountResponseDTO;
import ru.kondratenko.jse44.dto.ItemResponse;
import ru.kondratenko.jse44.dto.ListAccountResponseDTO;
import ru.kondratenko.jse44.entity.Account;

import java.util.Date;

public interface IAccountIService {
    AccountResponseDTO create(final Account item);
    ListAccountResponseDTO findAll();
    AccountResponseDTO findByAccountNumber(String accountNumber);
    AccountResponseDTO updateByAccountNumber( Account account);
    void removeByAccountNumber(String accountNumber);
    ListAccountResponseDTO findAccounts(Integer id, String firsName, String lastName, Date bDay, String account, Double balance);
    ItemResponse findCustomersAccounts(Integer id, String firsName, String lastName, Date bDay, String account, Double balance);
}
