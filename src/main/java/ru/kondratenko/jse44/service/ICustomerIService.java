package ru.kondratenko.jse44.service;

import ru.kondratenko.jse44.dto.*;

import java.util.Date;

public interface ICustomerIService {
    CustomerResponseDTO create(final CustomerDTO item);
    ListCustomerResponseDTO findAll();
    CustomerResponseDTO findById(Integer id);
    CustomerResponseDTO updateById(CustomerDTO customer);
    void removeById(Integer id);
    ListCustomerResponseDTO findCustomers(Integer id, String firsName, String lastName, Date bDay, String account, Double balance);
    ItemResponse findCustomersAccounts(Integer id, String firsName, String lastName, Date bDay, String account, Double balance);
}
