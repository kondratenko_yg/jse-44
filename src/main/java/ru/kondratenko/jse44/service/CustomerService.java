package ru.kondratenko.jse44.service;

import ru.kondratenko.jse44.dto.CustomerDTO;
import ru.kondratenko.jse44.dto.CustomerResponseDTO;
import ru.kondratenko.jse44.dto.ItemResponse;
import ru.kondratenko.jse44.dto.ListCustomerResponseDTO;
import ru.kondratenko.jse44.dto.mapper.CustomerDTOMapper;
import ru.kondratenko.jse44.entity.Customer;
import ru.kondratenko.jse44.entity.Item;
import ru.kondratenko.jse44.enumerated.Status;
import ru.kondratenko.jse44.repository.CustomerRepository;

import java.util.Date;
import java.util.Optional;

public  class CustomerService implements ICustomerIService {

    private static volatile CustomerService instance = null;

    private final CustomerRepository customerRepository = CustomerRepository.getInstance();

    private CustomerService() {
    }

    public static CustomerService getInstance() {
        CustomerService localInstance = instance;
        if (localInstance == null) {
            synchronized (CustomerService.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new CustomerService();
                }
            }
        }
        return localInstance;
    }

    @Override
    public CustomerResponseDTO create(final CustomerDTO customerDTO) {
        Customer customer1 = CustomerDTOMapper.fromDto(customerDTO);
        Optional<Customer> customerOptional = customerRepository.create(customer1);
        if (customerOptional.isPresent()) {
            return CustomerResponseDTO.builder().payloadCustomer(CustomerDTOMapper.toDto(customerOptional.get())).status(Status.OK).build();
        }
        return CustomerResponseDTO.builder().status(Status.DB_ERROR).build();
    }


    @Override
    public CustomerResponseDTO updateById(CustomerDTO customerDTO) {
        Optional<Customer> customer1 = customerRepository.findById(customerDTO.getId());
        if (customer1.isPresent()) {
            Customer updatedCustomer = CustomerDTOMapper.fromDto(customerDTO);
            customer1 = customerRepository.update(updatedCustomer);
            if (customer1.isPresent()) {
                return CustomerResponseDTO.builder().payloadCustomer(CustomerDTOMapper.toDto(customer1.get())).status(Status.OK).build();
            }
        }
        return CustomerResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public CustomerResponseDTO findById(Integer id) {
        Optional<Customer> customer1 = customerRepository.findById(id);
        if (customer1.isPresent()) {
            return CustomerResponseDTO.builder().payloadCustomer(CustomerDTOMapper.toDto(customer1.get())).status(Status.OK).build();
        }
        return CustomerResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public void removeById(Integer id) {
        customerRepository.removeById(id);
    }

    @Override
    public ListCustomerResponseDTO findAll() {
        return ListCustomerResponseDTO
                .builder()
                .payloadCustomer(customerRepository.findAll().stream().map(CustomerDTOMapper::toDto).toArray(CustomerDTO[]::new))
                .status(Status.OK).build();
    }

    @Override
    public ListCustomerResponseDTO findCustomers(Integer id, String firsName, String lastName, Date bDay, String account, Double balance) {
        return ListCustomerResponseDTO
                .builder()
                .payloadCustomer(customerRepository.findItems(id, firsName, lastName,  bDay,  account,  balance).stream().map(CustomerDTOMapper::toDto).toArray(CustomerDTO[]::new))
                .status(Status.OK).build();
    }
    @Override
    public ItemResponse findCustomersAccounts(Integer id, String firsName, String lastName, Date bDay, String account, Double balance) {
        return ItemResponse
                .builder()
                .payloadItem(customerRepository.findCombItems(id, firsName, lastName,  bDay,  account,  balance)
                        .toArray(Item[]::new)
                )
                .status(Status.OK).build();
    }
}
