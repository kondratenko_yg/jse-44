package ru.kondratenko.jse44.controller;

import ru.kondratenko.jse44.dto.*;
import ru.kondratenko.jse44.entity.Customer;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import jakarta.jws.soap.SOAPBinding;

import java.util.Date;

@WebService
@SOAPBinding(
        style = SOAPBinding.Style.RPC
)
public interface IControllerCustomer {
    @WebMethod
    CustomerResponseDTO create(final CustomerDTO item);
    @WebMethod
    ListCustomerResponseDTO findAll();
    @WebMethod
    CustomerResponseDTO findById(@WebParam(name = "id") Integer id);
    @WebMethod
    CustomerResponseDTO updateById(CustomerDTO customer);
    @WebMethod
    void removeById(@WebParam(name = "id") Integer id);
    @WebMethod
    ListCustomerResponseDTO findCustomers(
        @WebParam(name = "id") Integer id,
        @WebParam(name = "firsName") String firsName,
        @WebParam(name = "lastName") String lastName,
        @WebParam(name = "bDay") Date bDay,
        @WebParam(name = "account") String account,
        @WebParam(name = "balance") Double balance
    );
    @WebMethod
    ItemResponse findCustomersAccounts(@WebParam(name = "id") Integer id,
                                       @WebParam(name = "firsName") String firsName,
                                       @WebParam(name = "lastName") String lastName,
                                       @WebParam(name = "bDay") Date bDay,
                                       @WebParam(name = "account") String account,
                                       @WebParam(name = "balance") Double balance);
}
