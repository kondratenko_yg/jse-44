package ru.kondratenko.jse44.controller;

import ru.kondratenko.jse44.dto.AccountResponseDTO;
import ru.kondratenko.jse44.dto.ItemResponse;
import ru.kondratenko.jse44.dto.ListAccountResponseDTO;
import ru.kondratenko.jse44.entity.Account;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import jakarta.jws.soap.SOAPBinding;

import java.util.Date;

@WebService
@SOAPBinding(
        style = SOAPBinding.Style.RPC
)
public interface IControllerAccount {
    @WebMethod
    AccountResponseDTO create(Account account);
    @WebMethod
    ListAccountResponseDTO findAll();
    @WebMethod
    AccountResponseDTO findByAccountNumber(@WebParam(name = "accountNumber")String accountNumber);
    @WebMethod
    AccountResponseDTO updateByAccountNumber(Account account);
    @WebMethod
    void removeByAccountNumber(@WebParam(name = "accountNumber")String accountNumber);
    @WebMethod
    ListAccountResponseDTO findAccounts(
            @WebParam(name = "id") Integer id,
            @WebParam(name = "firsName") String firsName,
            @WebParam(name = "lastName") String lastName,
            @WebParam(name = "bDay") Date bDay,
            @WebParam(name = "account") String account,
            @WebParam(name = "balance") Double balance
    );
    @WebMethod
    ItemResponse findCustomersAccounts(@WebParam(name = "id") Integer id,
                                       @WebParam(name = "firsName") String firsName,
                                       @WebParam(name = "lastName") String lastName,
                                       @WebParam(name = "bDay") Date bDay,
                                       @WebParam(name = "account") String account,
                                       @WebParam(name = "balance") Double balance);
}
