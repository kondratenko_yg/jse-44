package ru.kondratenko.jse44.controller;

import jakarta.jws.WebService;
import ru.kondratenko.jse44.dto.CustomerDTO;
import ru.kondratenko.jse44.dto.CustomerResponseDTO;
import ru.kondratenko.jse44.dto.ItemResponse;
import ru.kondratenko.jse44.dto.ListCustomerResponseDTO;
import ru.kondratenko.jse44.service.CustomerService;

import java.util.Date;

@WebService(endpointInterface = "ru.kondratenko.jse44.controller.IControllerCustomer")
public class IControllerCustomerImpl implements IControllerCustomer {
    private final CustomerService customerService;

    public IControllerCustomerImpl() {
        this.customerService = CustomerService.getInstance();
    }

    @Override
    public CustomerResponseDTO create(CustomerDTO item) {
        return customerService.create(item);
    }

    @Override
    public ListCustomerResponseDTO findAll() {
        return customerService.findAll();
    }

    @Override
    public CustomerResponseDTO findById(Integer id) {
        return customerService.findById(id);
    }

    @Override
    public CustomerResponseDTO updateById(CustomerDTO customer) {
        return customerService.updateById(customer);
    }

    @Override
    public void removeById(Integer id) {
        customerService.removeById(id);
    }

    @Override
    public ListCustomerResponseDTO findCustomers(Integer id, String firsName, String lastName, Date bDay, String account, Double balance) {
        return customerService.findCustomers(id, firsName, lastName,  bDay,  account,  balance);
    }
    @Override
    public ItemResponse findCustomersAccounts(Integer id, String firsName, String lastName, Date bDay, String account, Double balance) {
        return customerService.findCustomersAccounts(id, firsName, lastName,  bDay,  account,  balance);
    }
}
