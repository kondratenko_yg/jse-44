package ru.kondratenko.jse44.controller;

import ru.kondratenko.jse44.dto.AccountResponseDTO;
import ru.kondratenko.jse44.dto.ItemResponse;
import ru.kondratenko.jse44.dto.ListAccountResponseDTO;
import ru.kondratenko.jse44.entity.Account;
import ru.kondratenko.jse44.service.AccountService;

import jakarta.jws.WebService;

import java.util.Date;

@WebService(endpointInterface = "ru.kondratenko.jse44.controller.IControllerAccount")
public class IControllerAccountImpl implements IControllerAccount {
    private final AccountService accountService;


    public IControllerAccountImpl() {
        this.accountService = AccountService.getInstance();
    }

    @Override
    public AccountResponseDTO create(Account account) {
        return accountService.create(account);
    }

    @Override
    public ListAccountResponseDTO findAll() {
        return accountService.findAll();
    }

    @Override
    public AccountResponseDTO findByAccountNumber(String accountNumber) {
        return accountService.findByAccountNumber(accountNumber);
    }

    @Override
    public AccountResponseDTO updateByAccountNumber(Account account) {
        return accountService.updateByAccountNumber(account);
    }

    @Override
    public void removeByAccountNumber(String accountNumber) {
        accountService.removeByAccountNumber(accountNumber);
    }

    @Override
    public ListAccountResponseDTO findAccounts(Integer id, String firsName, String lastName, Date bDay, String account, Double balance) {
        return accountService.findAccounts(id, firsName, lastName,  bDay,  account,  balance);
    }

    @Override
    public ItemResponse findCustomersAccounts(Integer id, String firsName, String lastName, Date bDay, String account, Double balance) {
        return accountService.findCustomersAccounts(id, firsName, lastName,  bDay,  account,  balance);
    }

}

