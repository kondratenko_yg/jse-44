package ru.kondratenko.jse44.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Item {
    private Integer id;

    private String firstName;

    private String lastName;

    private Date birthDate ;

    private List<Account> account ;

}
