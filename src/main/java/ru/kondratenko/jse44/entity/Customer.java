package ru.kondratenko.jse44.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Customer implements Serializable {
    public static final Long serialVersionUID = 1L;

    private Integer id;

    private String firstName;

    private String lastName;

    private Date birthDate ;
}
